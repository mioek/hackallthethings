#!/bin/bash
#
# THIS IS THE ORIGINAL CODE. IT WAS REPOSTED ELSEWHERE BY OTHERS
#
# I DO NOT HOLD RESPONSIBILITY FOR THOSE WHO USED THIS SCRIPT FOR
# BREAKING LAWS OR ANY OTHER REASON. THIS CODE WAS CREATED TO
# ASSIST RESEARCHERS ON ASHLEY MADISON DUMPS WHICH WERE AVAILABLE
# ON THE TORRENT SITES.
#
# I DO NOT STORE OR DISTRIBUTE ANY INFORMATION FROM THE DUMPS.
# THIS CODE WAS CREATED BASED ON THE INFO FROM ARS TECHNICA AND
# /R/NETSEC, THAT AVAILABLE ON THE INTERNET.
#
# THE MD5 HASHES ARE RELATED TO THE BCRYPT.
# THEY'RE USED TO DETERMINE BCRYPT FUNCTION OF THE PASSWORDS.
# http://cynosureprime.blogspot.com/2015/09/how-we-cracked-millions-of-ashley.html
#
menu(){
echo ""
echo " put this bash file into the first AM dump directory"
echo ""
echo "  [1] Email"
echo "  [2] bcrypt"
echo "  [3] MD5"
echo "  [0] exit"
echo ""
echo -n "Select option: "
read an

if [ "$an" = "1" ]; then
	echo -n "enter file location:"
	read lo
	egrep -o "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}\\b" $lo > AM-email.txt
	echo "email saved at AM-email.txt"
	menu

elif [ "$an" = "2" ]; then
	echo -n "enter file location:"
	read lo
	egrep -o '\$2a\$12\$[./A-Za-z0-9]{53}' $lo > AM-bcrypt.txt
	echo "bcrypt saved at AM-bcrypt.txt"
	menu

elif [ "$an" = "3" ]; then
	echo -n "enter file location:"
	read lo
	egrep -o '[0-9a-f]\{32\}' $lo > AM-md5.txt
	echo "md5 hash saved at AM-md5.txt"
	menu

elif [ "$an" = "0" ]; then
	exit

else
	menu

fi
}
menu
