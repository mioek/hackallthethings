#!/usr/bin/perl
use warnings;
use strict;
my $count = 1;
while ($count <= 100) {
        my $fizz = 1;
        my $buzz = 1;
        my $div3 = 1;
        my $div5 = 1;
        $div3 = $count/3;
        $div5 = $count/5;
        if ($div3 !~ /\./) {
                $fizz = "Fizz";
        };
        if ($div5 !~ /\./) {
                $buzz = "Buzz";
        };
        if ($fizz =~ /Fizz/) {
                print "Fizz";
        };
        if ($buzz =~ /Buzz/) {
                print "Buzz";
        };
        if ($fizz !~ /Fizz/) {
                if ($buzz !~ /Buzz/) {
                        print $count;
                };
        };
        print "\n";
        ++$count;
}
