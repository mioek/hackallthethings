#!/bin/bash
for i in {1..100}
do
        let "a = ($i % 3)"
        let "b = ($i % 5)"
        if [ $a -eq 0 ] && [ $b -eq 0 ] ; then
                echo "fizzbuzz"
        else
                if [ $a -eq 0 ] ; then
                        echo "fizz"
                else
                        if [ $b -eq 0 ] ; then
                                echo "buzz"
                        else
                                echo $i
                        fi
                fi
        fi
done
