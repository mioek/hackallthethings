#!/usr/bin/env python
for z in range(1, 101):
    if not(z % 3 or z % 5):
        print 'FizzBuzz'
    elif not z % 3:
        print 'Fizz'
    elif not z % 5:
        print 'Buzz'
    else:
        print z
